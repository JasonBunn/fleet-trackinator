# serializes the Route object for an JSON response containing the listed attributes
class RouteSerializer < ActiveModel::Serializer
  attributes :id, :name, :schedualed
  has_one :user
  has_one :company
end
