# serializes the Account object for an JSON response containing the listed attributes
class AccountSerializer < ActiveModel::Serializer
  attributes :id, :name, :number, :address, :city, :state, :zip, :suite, :notes, :latitude, :longitude
  has_one :company
end
