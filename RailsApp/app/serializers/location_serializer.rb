# serializes the Location object for an JSON response containing the listed attributes
class LocationSerializer < ActiveModel::Serializer
  attributes :id, :latitude, :longitude
  has_one :user
  has_one :company
end
