# serializes the Company object for an JSON response containing the listed attributes
class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :state, :city, :zip, :number, :latitude, :longitude
end
