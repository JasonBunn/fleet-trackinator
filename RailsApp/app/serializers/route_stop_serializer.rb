# serializes the RouteStop object for an JSON response containing the listed attributes
class RouteStopSerializer < ActiveModel::Serializer
  attributes :id
  has_one :route
  has_one :account
  has_one :user
  has_one :company
end
