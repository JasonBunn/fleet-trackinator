json.array!(@route_stops) do |route_stop|
  json.extract! route_stop, :id, :route_id, :account_id, :user_id, :company_id
  json.url route_stop_url(route_stop, format: :json)
end
