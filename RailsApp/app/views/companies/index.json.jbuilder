json.array!(@companies) do |company|
  json.extract! company, :id, :name, :address, :state, :city, :zip, :number, :latitude, :longitude
  json.url company_url(company, format: :json)
end
