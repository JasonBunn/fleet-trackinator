json.extract! @account, :id, :name, :number, :address, :city, :state, :zip, :suite, :notes, :latitude, :longitude, :user_id, :created_at, :updated_at
