json.array!(@accounts) do |account|
  json.extract! account, :id, :name, :number, :address, :city, :state, :zip, :suite, :notes, :latitude, :longitude, :user_id
  json.url account_url(account, format: :json)
end
