json.array!(@routes) do |route|
  json.extract! route, :id, :name, :schedualed, :user_id, :company_id
  json.url route_url(route, format: :json)
end
