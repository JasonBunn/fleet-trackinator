# defines a Company as it relates to other objects
class Company < ActiveRecord::Base
has_many :user
has_many :account
has_paper_trail
geocoded_by :full_address
before_validation :geocode
paginates_per 15

validates_with LocationValidator

# returns the full address of the given Company
  def full_address
    "#{self.address}, #{self.city}, #{self.state} #{self.zip}" 
  end
  
  # returns a string representing the onClick action
  def get_click
    "lat: #{self.latitude}, lng: #{self.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{self.name}'"
  end
  
  # checks to see if the company has a valid location or not
  def has_valid_location?
    !latitude.nil? && !longitude.nil?
  end

end
