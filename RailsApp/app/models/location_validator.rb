
# validates that Geocoding returns a lat and long from given address
class LocationValidator < ActiveModel::Validator
  def validate(record)
    if !record.has_valid_location?
      record.errors[:base] << "That is not a valid Address or Location"
    end
  end
end