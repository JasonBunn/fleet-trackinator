
# Defines the ability or access each type of user will have
class Ability
  include CanCan::Ability
  
    def initialize(user)
      user ||= User.new # guest user
      can :read, :all
      if user.role? :admin # full access
        can :manage, :all
        can :access, :all       # only allow admin users to access Rails Admin
        can :dashboard, :all            # allow access to dashboard
        
      elsif user.role? :manager # access within a defined company
        can :manage, Account
        can :manage, Users 
      end
    end
end
