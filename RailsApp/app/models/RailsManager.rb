module RailsManager
  module Extensions
    module CanCan
      class AuthorizationAdapter
        def authorize(action, abstract_model = nil, model_object = nil)
          @controller.current_ability.authorize!(action, model_object || abstract_model && model_name(abstract_model.model)) if action
        end
        def authorized?(action, abstract_model = nil, model_object = nil)
          @controller.current_ability.can?(action, model_object || abstract_model && model_name(abstract_model.model)) if action
        end
        private
        def model_name(model)
          model.to_s.underscore.pluralize.to_sym
        end
      end
    end
  end
end
