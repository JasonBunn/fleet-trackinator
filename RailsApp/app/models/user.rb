# defines a User and its relations to other objects
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_paper_trail
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  has_and_belongs_to_many :roles
  belongs_to :company
  has_many :account
  
  before_save :setup_role
  paginates_per 15
  
  
  #geocoded_by :current_sign_in_ip,
  #  :latitude => :lat, :longitude => :lon
  #after_validation :geocode 

  # returns the defined role of the given User
  def role?(role)
      return !!self.roles.find_by_name(role.to_s.camelize)
  end

  # Default role is "User"
  def setup_role 
    if self.role_ids.empty?     
      self.role_ids = [3] 
    end
  end
  
  # returns the full name first and last of the given User  
  def full_name
    "#{self.first_name} #{self.last_name}" 
  end

  # returns the location history of the user
  def location_history
    @locations = Location.where('user_id=?', id).order('created_at desc').all()
  end
  
  
end
