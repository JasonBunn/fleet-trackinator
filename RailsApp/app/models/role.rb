# defines Role and its relation to other objects
class Role < ActiveRecord::Base
  has_and_belongs_to_many :users
end