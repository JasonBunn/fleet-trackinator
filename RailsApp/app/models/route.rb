# defines a route and its relation to other objects
class Route < ActiveRecord::Base
  belongs_to :user
  belongs_to :company


  # Gets the locations of route stops belonging to this route and returns them in JSON format
  def get_locations
  	locations = Array.new

  	# Find all of the route stops with this route id
  	@stops = RouteStop.find(:all, :conditions => [ "route_id = ?", self.id])

  	@stops.each do |stop|
  		# Push the location from the route stop itself
  		locations.push stop.get_location
  	end

  	# Hand it back in JSON format
  	locations.to_json
  end
end
