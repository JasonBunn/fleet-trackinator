# Defines Location and its relation to other objects
class Location < ActiveRecord::Base
  belongs_to :user
  belongs_to :company
  paginates_per 10
  
    # returns a string representing the onClick action
  def get_click
    "lat: #{self.latitude}, lng: #{self.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{self.updated_at}'"
  end
  
  # checks to see if the account has a valid location or not
  def has_valid_location?
    !latitude.nil? && !longitude.nil?
  end
end
