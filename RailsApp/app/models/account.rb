
# defines a Account as it relates to other objects
class Account < ActiveRecord::Base
  belongs_to :company
  belongs_to :user
  has_paper_trail
  geocoded_by :full_address
  before_validation :geocode
  paginates_per 15
  
  validates_with LocationValidator

 # returns the full address of the given Account
  def full_address
  	"#{self.address}, #{self.city}, #{self.state} #{self.zip}" 
  end

  # returns a string representing the onClick action
  def get_click
  	"lat: #{self.latitude}, lng: #{self.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{self.name}'"
  end

  # checks to see if the account has a valid location or not
  def has_valid_location?
    !latitude.nil? && !longitude.nil?
  end
  
end
