# Gives the Manager access to users
class ManagerAbility
  include CanCan::Ability
  def initialize(user)
    if user.role? :manager
      can :manage, Users
    end
  end
end
