# defines a RouteStop and its relation to other objects
class RouteStop < ActiveRecord::Base
  belongs_to :route
  belongs_to :account
  belongs_to :user
  belongs_to :company

  # Gets the location object for a route stop
  def get_location
  	# Find the associated account
  	@account = Account.find(self.account_id)
  	# Hand back a nicely formatted shorthand account location object
  	{ name: @account.name, lat: @account.latitude, lng: @account.longitude }
  end
end
