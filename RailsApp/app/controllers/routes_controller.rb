class RoutesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_route, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  # Displays a list of all Routes
  def index
    
    if (params[:user_id] != nil)
          @user = User.find(params[:user_id])
          @search = Route.where('routes.User_id' => params[:user_id]).search(params[:q])
        end
        
        # Check to see if the user is a Administrator , if so show all accounts
        if can? :manage, :all
          if (@search == nil)
            @company = Company.find(current_user.company_id)
            @search = Route.all.search(params[:q])
          end
    
        # Check to see if the user is a Manager, if so show all accounts from company
        elsif can? :manage, Account
          if (@search == nil)
            @company = Company.find(current_user.company_id)
            @search = Route.where('routes.Company_id' => current_user.company_id).search(params[:q])
          end
            
        #otherwise show only my account info.
        else 
          @user = User.find(current_user.id)
          @search = Route.where('routes.User_id' => current_user.id).search(params[:q])
        end
    
        @routes = @search.result.page(params[:page]).order('created_at DESC')
        
        @search.build_condition
        @search.build_sort if @search.sorts.empty?  
    
          
    respond_with(@routes)
    
    
    
  end
  
  # displays the Route with the given Route ID
  def show
    # show the stops that are part of this route.
    @route_stops = RouteStop.where('route_stops.route_id' => @route.id)
    respond_with(@route)
  end
  
  # Displays the new Route form to allow the user to enter the Route info
  def new
    @route = Route.new
    @users = User.where('users.Company_id' => current_user.company_id)
    respond_with(@route)
  end
  
  # Displays the Route form to allow the user to edit the Account info
  def edit
    @users = User.where('users.Company_id' => current_user.company_id)
  end
  
  # creates a new Route object with the given Parameters
  def create
    @route = Route.new(route_params)
     respond_to do |format|
         if @route.save
           flash[:notice] = flash[:notice].to_a.concat @route.errors.full_messages
            # The system will auto forward user to add a new stop once the route is first created.
           format.html { redirect_to new_route_stop_path(:route_id => @route.id), :notice => 'Route was successfully created.' }
           format.json { render :json => @route, :status => :created, :location => @route }
         else
           flash[:notice] = flash[:notice].to_a.concat @route.errors.full_messages
           format.html { render :action => "new"}
           format.json { render :json => @route.errors, :status => :unprocessable_entity }
         end
       end
  end
  
  # Updates the given Route with the given parameters from the edit form
  def update
    @route.update(route_params)
    respond_with(@route)
  end
  
  # deletes the given Route object and destroys associated route_stops if the route
  # was successfully destroyed
  def destroy
    id = @route.id
    @route.destroy
    if @route.destroyed?
      RouteStop.where(route_id: id).destroy_all
    end
    respond_with(@route)
  end

  private
    def set_route
      @route = Route.find(params[:id])
    end

    def route_params
      params.require(:route).permit(:name, :scheduled, :user_id, :company_id)
    end
end
