class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_filter do
      resource = controller_path.singularize.gsub('/', '_').to_sym
      method = "#{resource}_params"
      params[resource] &&= send(method) if respond_to?(method, true)
    end
  
  
  # We may or may not want the user to have to authenticate right away.
  before_action :authenticate_user!
  
  rescue_from CanCan::AccessDenied do |exception|
      redirect_to '/', :alert => exception.message
    end
    
  
    def has_role?(current_user, role)
      return !!current_user.roles.find_by_name(role.to_s.camelize)
    end
  
    
end
