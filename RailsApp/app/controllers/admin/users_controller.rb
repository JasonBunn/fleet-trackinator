class Admin::UsersController < Admin::AdminController
  load_and_authorize_resource skip_load_resource only: [:create] 
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  
  before_filter do
      resource = controller_name.singularize.to_sym
      method = "#{resource}_params"
      params[resource] &&= send(method) if respond_to?(method, true)
  end
    
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all.page(params[:page]).order('created_at DESC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.attributes = params[:user]
    @user.role_ids = params[:user][:role_ids] if params[:user]
    respond_to do |format|
      if @user.save
        flash[:notice] = flash[:notice].to_a.concat @user.errors.full_messages
        format.html { redirect_to admin_users_path, :notice => 'User was successfully created.' }
        format.json { render :json => @user, :status => :created, :location => @user }
      else
        flash[:notice] = flash[:notice].to_a.concat @user.errors.full_messages
        format.html { render :action => "new"}
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end
 
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_path, :notice => 'User was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to admin_users_url }
      format.json { head :ok }
    end
  end

  # Sets the current users location
  def location
    # Get the current user
    @user = current_user
    
    # Update the users longitude and the latitude from the parameters
    @user.update_attribute(:latitude, params[:lat])
    @user.update_attribute(:longitude, params[:lng])
    
    if(params[:lat] != nil)
      @location = Location.new
      @location.user_id = current_user.id
      @location.company_id = current_user.company_id
      @location.latitude = @user.latitude
      @location.longitude =@user.longitude
      @location.save
    end
  end

  private
  def set_user
      @user = User.find(params[:id])
    end
  
  def user_params
   params.require(:user).permit(:id, :authenticity_token, :first_name, :last_name, :email, :password, :company_id, :password_confirmation, :current_password, :role_ids => [])
  end
end