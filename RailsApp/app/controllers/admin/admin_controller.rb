class Admin::AdminController < ApplicationController
  layout "admin"
    before_filter :verify_admin
    
    # Redirects user to Index page unless authenticated as having admin role
    def verify_admin
      :authenticate_user!
      redirect_to root_url unless has_role?(current_user, 'admin')
    end
    
    # defines current_ability for user as AdminAbility
    def current_ability
      @current_ability ||= AdminAbility.new(current_user)
    end
end