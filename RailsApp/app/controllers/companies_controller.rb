class CompaniesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  respond_to :html

  rescue_from CanCan::AccessDenied do |exception|
     redirect_to main_app.root_path, :alert => exception.message
   end
   
  # displays a list of all Companies in the DB in the order they were added
  def index
    @companies = Company.all.page(params[:page]).order('created_at DESC')
    respond_with(@companies)
  end

  # Displays the info for the Company with the Given id
  def show
    respond_with(@company)
  end

  # Displays the new Company form to allow the user to enter the Company info
  def new
    @company = Company.new
    respond_with(@company)
  end
  
  # Displays the Company form to allow the user to edit the Company info
  def edit
  end
  
  # creates a new Company object with the given Parameters
  def create
    @company = Company.new(company_params)
    @company.save
    respond_with(@company)
  end
  
  # Updates the given Company with the given parameters from the edit form
  def update
    @company.update(company_params)
    respond_with(@company)
  end
  
  # deletes the given Company object from the DB
  def destroy
    @company.destroy
    respond_with(@company)
  end

  private
    def set_company
      @company = Company.find(params[:id])
    end

    def company_params
      params.require(:company).permit(:name, :address, :state, :city, :zip, :number, :latitude, :longitude)
    end
end
