class MapController < ApplicationController
  before_action :authenticate_user!
  
  # Displays a map with the given markers in @hash, and the given routes
  def index
    @user = User.find(current_user.id)
    @users = User.all
    @hash = Gmaps4rails.build_markers(@users) do |user, marker|
      location = request.location
      
      #displays marker in Atlanta for development
      #marker.lat 33.748600
      #marker.lng -84.388400
      
      #This will display map with marker centered on current users geolocation
      #in production environment
      marker.lat request.location.latitude
      marker.lng request.location.longitude
      
      marker.infowindow @user.full_name
    end
  end
  
end
