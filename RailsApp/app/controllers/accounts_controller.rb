class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :set_company
  respond_to :html

  # Displays a list of Accounts that the current user is authorized to view
  def index
    # check to see if your trying to view specific user's account
    # if so, search on that id.
    if (params[:user_id] != nil)
      @user = User.find(params[:user_id])
      @search = Account.where('accounts.User_id' => params[:user_id]).search(params[:q])
    end
    
    # Check to see if the user is a Administrator , if so show all accounts
    if can? :manage, :all
      if (@search == nil)
        @company = Company.find(current_user.company_id)
        @search = Account.all.search(params[:q])
      end

    # Check to see if the user is a Manager, if so show all accounts from company
    elsif can? :manage, Account
      if (@search == nil)
        @company = Company.find(current_user.company_id)
        @search = Account.where('accounts.Company_id' => current_user.company_id).search(params[:q])
      end
        
    #otherwise show only my account info.
    else 
      @user = User.find(current_user.id)
      @search = Account.where('accounts.User_id' => current_user.id).search(params[:q])
    end

    @accounts = @search.result.page(params[:page]).order('created_at DESC')
    
    @search.build_condition
    @search.build_sort if @search.sorts.empty?  

      
    respond_with(@accounts)

  end
  
  # displays the Account with the given Account ID
  def show
    respond_with(@account)
  end

  # Displays the new Account form to allow the user to enter the Account info
  def new
    @account = Account.new
    @users = User.where('users.Company_id' => current_user.company_id)
    respond_with(@account)
  end

  # Displays the Account form to allow the user to edit the Account info
  def edit
    @users = User.where('users.Company_id' => current_user.company_id)

  end
  
  # creates a new Account object with the given Parameters
  def create
      @account = Account.new(account_params)
      @account.save
      respond_with(@account)
  end

  # Updates the given Account with the given parameters from the edit form
  def update
    @account.update(account_params)
    respond_with(@account)
  end

  # deletes the given Account object from the DB
  def destroy
    @account.destroy
    respond_with(@account)
  end

  # adds the account to the current map
  def map
    @account.map
  end

  private
    def set_account
      @account = Account.find(params[:id])
    end
    
    def set_company
      @company = Company.find(current_user.company_id)
    end
    
    def account_params
      params.require(:account).permit(:name, :number, :address, :city, :state, :zip, :suite, :notes, :latitude, :longitude, :company_id, :user_id)
    end
end
