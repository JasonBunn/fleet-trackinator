class Manager::ManagerController < ApplicationController
    before_filter :verify_manager
    
    # Redirects user to Index page unless authenticated as having admin role
    def verify_manager
      :authenticate_user!
      redirect_to root_url unless has_role?(current_user, 'manager')
    end
    
    # defines current_ability for user as ManagerAbility
    def current_ability
      @current_ability ||= ManagerAbility.new(current_user)
    end
end