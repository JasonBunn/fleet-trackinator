# Root controller of the App
class StaticController < ApplicationController
  skip_before_filter :authenticate_user!
  
  # Displays the Home/Index/Root page of the App
  def index
    #render :file => 'public/index.html'
  end
  
  # Displays the About page
  def about
  end
  
end
