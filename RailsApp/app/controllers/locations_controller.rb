class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  # Displays a list of Locations that the current user is authorized to view
  def index
      # Check to see if the user is a Admin, if so show me all locations
    if can? :manage, :all
      @search = Location.all.search(params[:q])

    # Check to see if the user is a Manager, if so show me all locations for that company
    elsif can? :manage, Account
      @search = Location.where('locations.Company_id' => current_user.company_id).search(params[:q])

    # Otherwise show me only my locations
    else 
      @search = Location.where('locations.User_id' => current_user.id).search(params[:q])
    end
    @locations = @search.result.page(params[:page]).order('created_at DESC')
    
    @search.build_condition
    @search.build_sort if @search.sorts.empty? 
    
    respond_with(@locations)
  end
  
  # displays the Location with the given Location ID
  def show
    respond_with(@location)
  end
  
  # Displays the new Location form to allow the user to enter the Location info
  def new
    @location = Location.new
    respond_with(@location)
  end
  
  # Displays the Location form to allow the user to edit the Location info
  def edit
  end
  
  # creates a new Location object with the given Parameters
  def create
    @location = Location.new(location_params)
    @location.save
    respond_with(@location)
  end
  
  # Updates the given Location with the given parameters from the edit form
  def update
    @location.update(location_params)
    respond_with(@location)
  end
  
  # deletes the given Location object from the DB
  def destroy
    @location.destroy
    respond_with(@location)
  end

  private
    def set_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.require(:location).permit(:latitude, :longitude, :user_id, :company_id)
    end
end
