class RouteStopsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_route_stop, only: [:show, :edit, :update, :destroy]

  respond_to :html
  
  # Displays a list of Route_stops
  def index
    @route_stops = RouteStop.all
    respond_with(@route_stops)
  end
  
  # displays the Route_stop with the given Route_stop ID
  def show
    respond_with(@route_stop)
  end
  
  # Displays the new Route_stop form to allow the user to enter the Route_stop info
  def new
    @route_id = params[:route_id]
    @routes = Route.where('routes.id' => @route_id)
    @route = @routes.first(:order => 'user_id DESC')
    # this will make sure that only 1 route_stop is added per route.
    @current_stops = RouteStop.where('route_stops.route_id' => @route_id).pluck(:account_id)
    @stop = @current_stops.count
    # by default stop will be 0, and we want it to always start at 1. When they make a new one, it should always be the next number.
    @stop = @stop +1;
    @accounts = Account.where('accounts.User_id' => @current_user.id)
    # this will make sure that only 1 route_stop is added per route.
    if (@stop > 1)
      @accounts = @accounts.where('id not in (?)',@current_stops)
      
    end

    # since we are created a new route, the default stop number should be 1. so if there are no stops, its one. Otherwise add 1 to the amount of stops.
    
    # determines if there are any accounts not already added to route
    if @accounts.count > 0
      @has_account = true
    else
      @has_account = false
    end
        
    
    @route_stop = RouteStop.new
    respond_with(@route_stop)
  end
  
  # Displays the Route_stop form to allow the user to edit the Route_stop info
  def edit
    #Sets the Stop number
    @stop = @route_stop.stop
    #sets accounts still available for this route
    @accounts = Account.where('accounts.User_id' => @route.user_id)
    @has_account = true
  end
  
  # creates a new Route_stop object with the given Parameters
  def create
    @route_stop = RouteStop.new(route_stop_params)
    respond_to do |format|
             if @route_stop.save
               flash[:notice] = flash[:notice].to_a.concat @route_stop.errors.full_messages
               format.html { redirect_to route_path(@route_stop.route_id), :notice => 'Route Stop was successfully added.' }
               format.json { render :json => @route_stop, :status => :created, :location => @route_stop }
             else
               flash[:notice] = flash[:notice].to_a.concat @route_stop.errors.full_messages
               format.html { render :action => "new"}
               format.json { render :json => @route_stop.errors, :status => :unprocessable_entity }
             end
           end
  end
  
  # Updates the given Route_stop with the given parameters from the edit form
  def update
    @route_stop.update(route_stop_params)
    respond_with(@route_stop)
  end
  
  # deletes the given Route_stop object from the DB
  def destroy
    @route_stop.destroy
    respond_with(@route_stop)
  end

  private
    def set_route_stop
      @route_stop = RouteStop.find(params[:id])
      @route = Route.find(@route_stop.route_id)
      @route_id = params[:route_id]
    end

    def route_stop_params
      params.require(:route_stop).permit(:route_id, :account_id, :stop)
    end
end
