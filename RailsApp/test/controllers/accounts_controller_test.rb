require 'test_helper'

class AccountsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @company = companies(:one)
    @user = users(:one)
    sign_in @user
    @account = accounts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:accounts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create account" do
    assert_difference('Account.count') do
      post :create, account: { address: @account.address, city: @account.city, latitude: @account.latitude, longitude: @account.longitude, name: @account.name, notes: @account.notes, number: @account.number, state: @account.state, suite: @account.suite, user_id: @account.user_id, zip: @account.zip, company_id: @user.company_id }
    end

    assert_redirected_to account_path(assigns(:account))
  end

  test "should show account" do
    get :show, id: @account
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @account
    assert_response :success
  end

  test "should update account" do
    patch :update, id: @account, account: { address: @account.address, city: @account.city, latitude: @account.latitude, longitude: @account.longitude, name: @account.name, notes: @account.notes, number: @account.number, state: @account.state, suite: @account.suite, user_id: @account.user_id, zip: @account.zip }
    assert_redirected_to account_path(assigns(:account))
  end

  test "should destroy account" do
    assert_difference('Account.count', -1) do
      delete :destroy, id: @account
    end

    assert_redirected_to accounts_path
  end
end
