 require 'test_helper'

 class RoutesControllerTest < ActionController::TestCase
   include Devise::TestHelpers
   setup do
     @user = users(:one)
     sign_in @user
     @route = routes(:one)
   end

   test "should get index" do
     get :index
     assert_response :success
     assert_not_nil assigns(:routes)
   end

   test "should get new" do
     get :new
     assert_response :success
   end

   test "should create route" do
     assert_difference('Route.count') do
       @new_route = post :create, route: { company_id: @route.company_id, name: @route.name, scheduled: @route.scheduled, user_id: @route.user_id }
     end

     assert_redirected_to '/route_stops/new?route_id=' + Route.last.id.to_s
   end

   test "should show route" do
     get :show, id: @route
     assert_response :success
   end

   test "should get edit" do
     get :edit, id: @route
     assert_response :success
   end

   test "should update route" do
     patch :update, id: @route, route: { company_id: @route.company_id, name: @route.name, scheduled: @route.scheduled, user_id: @route.user_id }
     assert_redirected_to route_path(assigns(:route))
   end

   test "should destroy route" do
     assert_difference('Route.count', -1) do
       delete :destroy, id: @route
     end

     assert_redirected_to routes_path
   end
 end
