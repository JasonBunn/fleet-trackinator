 require 'test_helper'

 class MapControllerTest < ActionController::TestCase
   include Devise::TestHelpers
   setup do
     @user = users(:one)
     sign_in @user
     @location = locations(:one)
   end

   test "should get index" do
     get :index
     assert_response :success
     assert_not_nil assigns(:user)
   end

 end
