 require 'test_helper'

 class RouteStopsControllerTest < ActionController::TestCase
   include Devise::TestHelpers
   setup do
     @user = users(:one)
     sign_in @user
     @route_stop = route_stops(:one)
     @accounts = accounts(:one, :two, :three)
     @route = routes(:one)
   end

   test "should get index" do
     get :index
     assert_response :success
     assert_not_nil assigns(:route_stops)
   end

   test "should get new" do
     get :new, :route_id => @route.id
     assert_response :success
   end

   test "should create route_stop" do

     assert_difference('RouteStop.count') do
       post :create, route_stop: { account_id: @route_stop.account_id, route_id: @route_stop.route_id, stop: @route_stop.stop }
     end

     assert_redirected_to '/routes/' + @route_stop.route_id.to_s
   end

   test "should show route_stop" do
     get :show, id: @route_stop
     assert_response :success
   end

   test "should get edit" do
     get :edit, id: @route_stop
     assert_response :success
   end

   test "should update route_stop" do
     patch :update, id: @route_stop, route_stop: { route_id: @route_stop.route_id, account_id: @route_stop.account_id, stop: @route_stop.stop }
     assert_redirected_to route_stop_path(assigns(:route_stop))
   end

   test "should destroy route_stop" do
     assert_difference('RouteStop.count', -1) do
       delete :destroy, id: @route_stop
     end

     assert_redirected_to route_stops_path
   end
 end
