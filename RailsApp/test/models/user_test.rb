require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
    @user_2 = users(:two)
    @role = roles(:one)
    @location = locations(:two)
    @setup_user = User.new
  end
    
  test "setup role" do
    @user.role_ids = nil
    @user.setup_role
    assert_equal [3], @user.role_ids
  end

  test "get user full name" do
  	name = "#{@user.first_name} #{@user.last_name}"
  	assert_equal @user.full_name, name
  end
  
  test "location history" do
    @location_test = @user_2.location_history
    assert_equal @location.user_id, @location_test[0].user_id
  end
end
