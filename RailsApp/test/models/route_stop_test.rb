require 'test_helper'

class RouteStopTest < ActiveSupport::TestCase
  setup do
    @route_stop = route_stops(:one)
    @expected_account = accounts(:one)
  end
  
  test "get location" do
    @account = @route_stop.get_location
    assert_equal @account[:name], @expected_account.name
  end
  
end
