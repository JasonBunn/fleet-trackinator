require 'test_helper'

class CompanyTest < ActiveSupport::TestCase
  setup do
    @company = companies(:one)
  end
  
  # tests that company's full_address method returns the given company's full address
  test "get company full address" do
    address = "#{@company.address}, #{@company.city}, #{@company.state} #{@company.zip}"
    assert_equal @company.full_address, address
  end
  
  test "get_click returns string" do
    on_click_string = "lat: #{@company.latitude}, lng: #{@company.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{@company.name}'" 
    assert_equal @company.get_click, on_click_string
  end
  
  test "has valid location is true" do
    @company.longitude = -122.139453
    @company.latitude = 47.651044
    assert @company.has_valid_location?
  end
  
  test "has valid location is false" do
    @company.longitude = nil
    @company.latitude = nil
    assert !@company.has_valid_location?
  end
end
