require 'test_helper'

class LocationTest < ActiveSupport::TestCase
  setup do
    @location = locations(:one)
  end
  
  test "get_click returns string" do
    on_click_string = "lat: #{@location.latitude}, lng: #{@location.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{@location.updated_at}'" 
    assert_equal @location.get_click, on_click_string
  end
  
  test "has valid location is true" do
    @location.longitude = -122.139453
    @location.latitude = 47.651044
    assert @location.has_valid_location?
  end
  
  test "has valid location is false" do
    @location.longitude = nil
    @location.latitude = nil
    assert !@location.has_valid_location?
  end
end
