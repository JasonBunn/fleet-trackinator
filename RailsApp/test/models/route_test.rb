require 'test_helper'

class RouteTest < ActiveSupport::TestCase
  setup do
    location_1 = { name: accounts(:one).name, lat: accounts(:one).latitude, lng: accounts(:one).longitude }
    location_2 = { name: accounts(:two).name, lat: accounts(:two).latitude, lng: accounts(:two).longitude }
    location_3 = { name: accounts(:three).name, lat: accounts(:three).latitude, lng: accounts(:three).longitude }
    @locations = [ location_3, location_2, location_1 ]
    @route = routes(:one)
  end
  
  test "get locations" do
    assert_equal @locations.to_json, @route.get_locations
  end
  
end
