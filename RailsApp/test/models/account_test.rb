require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account = accounts(:one)
  end

  test "get account full address" do
  	full_addr = "#{@account.address}, #{@account.city}, #{@account.state} #{@account.zip}" 
  	assert_equal @account.full_address, full_addr
  end

  test "get_click returns string" do
    on_click_string = "lat: #{@account.latitude}, lng: #{@account.longitude}, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', title: '#{@account.name}'" 
    assert_equal @account.get_click, on_click_string
  end
  
  test "has valid location is true" do
    @account.longitude = -122.139453
    @account.latitude = 47.651044
    assert @account.has_valid_location?
  end
  
  test "has valid location is false" do
    @account.longitude = nil
    @account.latitude = nil
    assert !@account.has_valid_location?
  end
end
