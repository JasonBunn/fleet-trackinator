roles = Role.create([
  {id: 1, name: 'Admin'},
  {id: 2, name: 'Manager'},
  {id: 3, name: 'User'},
  {id: 4, name: 'Extra Role'}
]);

users = User.create([{first_name: 'William', last_name: 'Lloyd', email: 'wlloyd@westga.edu', password: 'westGaAdmin', password_confirmation: 'westGaAdmin', company_id: '1'},
    {first_name: 'Imani', last_name: 'Anthony', email: 'Cras.eget.nisi@semegestasblandit.co.uk', password: 'IAF29CGE8MO', password_confirmation: 'IAF29CGE8MO', company_id: '1'},
  {first_name: 'Gillian', last_name: 'May', email: 'quis.arcu@risusvarius.net', password: 'TGB99AHE9KQ', password_confirmation: 'TGB99AHE9KQ', company_id: '1'},
  {first_name: 'Gregory', last_name: 'Romero', email: 'ac.mattis@lectus.ca', password: 'TXH80MSS7KF', password_confirmation: 'TXH80MSS7KF', company_id: '1'},
  {first_name: 'Rhoda', last_name: 'Woods', email: 'sociis.natoque@turpisegestas.edu', password: 'HZU10PER1PQ', password_confirmation: 'HZU10PER1PQ', company_id: '1'},
  {first_name: 'Michael', last_name: 'Dixon', email: 'pede.malesuada.vel@nulla.co.uk', password: 'BXY17UCM9PF', password_confirmation: 'BXY17UCM9PF', company_id: '1'},
  {first_name: 'Eve', last_name: 'Cruz', email: 'a.ultricies@scelerisquescelerisque.ca', password: 'ZGT79HEY5AY', password_confirmation: 'ZGT79HEY5AY', company_id: '2'},
  {first_name: 'Katell', last_name: 'Saunders', email: 'pede@maurisrhoncus.com', password: 'HCV39XDX9UN', password_confirmation: 'HCV39XDX9UN', company_id: '2'},
  {first_name: 'Blaine', last_name: 'Wolfe', email: 'pede.nec@enimcondimentum.org', password: 'VJM89QRZ4OS', password_confirmation: 'VJM89QRZ4OS', company_id: '2'},
  {first_name: 'Quinlan', last_name: 'George', email: 'Nam.nulla.magna@Nuncut.co.uk', password: 'BIT87TVP1LJ', password_confirmation: 'BIT87TVP1LJ', company_id: '2'},
  {first_name: 'Maxine', last_name: 'Berry', email: 'sem.mollis.dui@interdumfeugiatSed.org', password: 'ICE79ZPQ2NC', password_confirmation: 'ICE79ZPQ2NC', company_id: '2'},
  {first_name: 'Glenna', last_name: 'Sullivan', email: 'Donec.feugiat.metus@mifelisadipiscing.com', password: 'GCN08BLL1QY', password_confirmation: 'GCN08BLL1QY', company_id: '3'},
  {first_name: 'Xaviera', last_name: 'Hoffman', email: 'In@tempusmauriserat.ca', password: 'EIT97RYS7NR', password_confirmation: 'EIT97RYS7NR', company_id: '3'},
  {first_name: 'Sonia', last_name: 'Shaw', email: 'luctus.vulputate.nisi@Sedmalesuada.org', password: 'LRR32OIY3YY', password_confirmation: 'LRR32OIY3YY', company_id: '3'},
  {first_name: 'Nolan', last_name: 'Schmidt', email: 'eu@dapibusquam.net', password: 'XDT51MWI2WS', password_confirmation: 'XDT51MWI2WS', company_id: '3'},
  {first_name: 'Ruby', last_name: 'Maynard', email: 'mus.Aenean@Nunc.org', password: 'QRY47YND3YV', password_confirmation: 'QRY47YND3YV', company_id: '4'},
  {first_name: 'Callum', last_name: 'Hudson', email: 'sodales.elit.erat@lectusquismassa.edu', password: 'CAD66PMH4FQ', password_confirmation: 'CAD66PMH4FQ', company_id: '4'},
  {first_name: 'Jillian', last_name: 'Baird', email: 'Cras.vehicula@nunc.ca', password: 'CNP06GKS5HR', password_confirmation: 'CNP06GKS5HR', company_id: '4'},
  {first_name: 'Kylynn', last_name: 'Rodriquez', email: 'vitae.erat.vel@Pellentesque.net', password: 'SWB88VEF2YJ', password_confirmation: 'SWB88VEF2YJ', company_id: '4'},
  {first_name: 'Jaden', last_name: 'Spencer', email: 'arcu.Aliquam@vulputateveliteu.ca', password: 'TEO14RCP3QO', password_confirmation: 'TEO14RCP3QO', company_id: '4'},
  {first_name: 'Shay', last_name: 'Cotton', email: 'adipiscing@Infaucibus.net', password: 'CHX76XJC6YT', password_confirmation: 'CHX76XJC6YT', company_id: '5'},
  {first_name: 'Ferris', last_name: 'Berry', email: 'est.Nunc.laoreet@Nuncpulvinararcu.com', password: 'HRL93KSH3HC', password_confirmation: 'HRL93KSH3HC', company_id: '5'},
  {first_name: 'Donovan', last_name: 'Vincent', email: 'quis.lectus@pharetrasedhendrerit.co.uk', password: 'PXW74CSW8YR', password_confirmation: 'PXW74CSW8YR', company_id: '5'},
  {first_name: 'Lynn', last_name: 'Mcguire', email: 'ut.quam.vel@apurus.com', password: 'UAW99RNP1YU', password_confirmation: 'UAW99RNP1YU', company_id: '5'},
  {first_name: 'Omar', last_name: 'Haynes', email: 'Proin@eros.ca', password: 'GNB55QJL2YD', password_confirmation: 'GNB55QJL2YD', company_id: '5'},
  {first_name: 'Callum', last_name: 'William', email: 'porttitor.tellus.non@urnaet.org', password: 'ERY09AMC5XY', password_confirmation: 'ERY09AMC5XY', company_id: '5'},
  {first_name: 'Fritz', last_name: 'Leon', email: 'Nulla@atarcuVestibulum.co.uk', password: 'CMQ75SGQ0RS', password_confirmation: 'CMQ75SGQ0RS', company_id: '6'},
  {first_name: 'Vielka', last_name: 'Lindsey', email: 'Nunc@etrutrum.edu', password: 'XKP81WAV7TW', password_confirmation: 'XKP81WAV7TW', company_id: '6'},
  {first_name: 'Sylvia', last_name: 'Vincent', email: 'amet.consectetuer@vulputateullamcorpermagna.edu', password: 'CDH49PES5XT', password_confirmation: 'CDH49PES5XT', company_id: '6'},
  {first_name: 'Madeson', last_name: 'Fletcher', email: 'mattis.velit@bibendum.com', password: 'BEF51DYI1BA', password_confirmation: 'BEF51DYI1BA', company_id: '6'},
  {email: 'brian.silberberg@gmail.com', password: 'BrianBrian', password_confirmation: 'BrianBrian', sign_in_count: 3, current_sign_in_at: "2015-07-06 19:02:56", last_sign_in_at: "2015-07-03 23:34:32", current_sign_in_ip: "166.170.32.63", last_sign_in_ip: "96.244.197.248", first_name: "Brian", last_name: "Silberberg", company_id: 1}
  ]);  

Account.create!([
  {name: "Dolor LLP", number: "(531) 828-2552", address: "3100 Quisque Ave", city: "Saint Louis", state: "Missouri", zip: "16055", suite: nil, notes: nil, latitude: "40.71416", longitude: "-79.751203", user_id: 31, company_id: 1},
  {name: "Microsoft", number: "(425) 882-8080", address: "1 Microsoft Way", city: "Redmond", state: "Washington", zip: "98052", suite: nil, notes: nil, latitude: "47.639583", longitude: "-122.128381", user_id: 31, company_id: 1},
  {name: "Googleplex", number: "(650) 253-0000", address: "1600 Amphitheatre Pkwy", city: "Mountain View", state: "California", zip: "94043", suite: nil, notes: nil, latitude: "37.422346", longitude: "-122.084189", user_id: 1, company_id: 1},
  {name: "Apple Inc.", number: "(564) 293-3038", address: "1 Infinite Loop", city: "Cupertino", state: "California", zip: "59114", suite: nil, notes: nil, latitude: "37.331712", longitude: "-122.030184", user_id: 18, company_id: 1},
  {name: "Pixar Animation Studios", number: "(510) 922-3000", address: "1200 Park Ave", city: "Emeryville", state: "California", zip: "94608", suite: nil, notes: nil, latitude: "37.83286", longitude: "-122.284003", user_id: 31, company_id: 1},
  {name: "Nintendo", number: "(425) 882-2040", address: "4600 150th Ave NE", city: "Redmond", state: "Washington", zip: "98052", suite: nil, notes: nil, latitude: "47.651044", longitude: "-122.139453", user_id: 31, company_id: 1},
  {name: "World Trade Center", number: "(800) 555-1212", address: "1 World Trade Center", city: "New York", state: "New York", zip: "10007", suite: nil, notes: nil, latitude: "40.713008", longitude: "-74.013169", user_id: 1, company_id: 1},
  {name: "GE Building", number: "(800) 555-1212", address: "30 Rockefeller Plaza", city: "New York", state: "New York", zip: "10112", suite: nil, notes: nil, latitude: "40.758943", longitude: "-73.979356", user_id: 1, company_id: 1},
  {name: "Ghostbusters", number: "(212) 555-2368", address: "14 N Moore Street", city: "NY", state: "New York", zip: "10013", suite: nil, notes: nil, latitude: "40.719644", longitude: "-74.006621", user_id: 1, company_id: 1},
  {name: "Cheers", number: "", address: "84 Beacon Street", city: "Boston", state: "Massachusetts", zip: "02108", suite: "", notes: "", latitude: "42.355939", longitude: "-71.071246", user_id: 1, company_id: 1},
  {name: "Major Nelson & Jeannie", number: "(800) 555-1212", address: "1020 Palm Drive", city: "Cocoa Beach", state: "Florida", zip: "32952", suite: nil, notes: nil, latitude: "28.353108", longitude: "-80.664306", user_id: 1, company_id: 1},
  {name: "McAllister House", number: "(800) 555-1212", address: "671 Lincoln Avenue", city: "Winnetka", state: "Illinois", zip: "60093", suite: nil, notes: nil, latitude: "42.109713", longitude: "-87.733614", user_id: 1, company_id: 1},
  {name: "Metus Aliquam Erat LLP", number: "(303) 702-3534", address: "551-2610 Egestas St.", city: "Kearney", state: "Nebraska", zip: "93586", suite: nil, notes: nil, latitude: "34.65", longitude: "-118.22", user_id: 7, company_id: 2},
  {name: "Lobortis Consulting", number: "(295) 324-3714", address: "8291 Aptent Road", city: "Tampa", state: "Florida", zip: "38485", suite: nil, notes: nil, latitude: "35.325533", longitude: "-87.736961", user_id: 7, company_id: 2},
  {name: "Aliquet LLC", number: "(383) 563-6301", address: "930-1957 Ut St.", city: "Davenport", state: "Iowa", zip: "99163", suite: nil, notes: nil, latitude: "41.498338", longitude: "-90.668908", user_id: 7, company_id: 2},
  {name: "Blandit Company", number: "(177) 412-6659", address: "P.O. Box 911, 2171 Nunc St.", city: "Harrisburg", state: "Pennsylvania", zip: "41274", suite: nil, notes: nil, latitude: "37.867722", longitude: "-82.812892", user_id: 31, company_id: 2},
  {name: "Suscipit Nonummy LLP", number: "(733) 467-3019", address: "438-4653 Vivamus Av.", city: "Erie", state: "Pennsylvania", zip: "17877", suite: nil, notes: nil, latitude: "40.87774", longitude: "-76.667591", user_id: 31, company_id: 2},
  {name: "Sem Magna Nec Industries", number: "", address: "P.O. Box 829, 9855 Ornare, Street", city: "Mobile", state: "Alabama", zip: "35078", suite: "", notes: "", latitude: "33.320209", longitude: "-86.438974", user_id: 31, company_id: 31},
  {name: "Tempus Scelerisque Lorem Industries", number: "", address: "8976 Phasellus St.", city: "Kearney", state: "Nebraska", zip: "75137", suite: "", notes: "", latitude: "32.635086", longitude: "-96.915547", user_id: 31, company_id: 31},
  {name: "Integer Vitae Nibh Consulting", number: "", address: "4982 Fusce St.", city: "Virginia Beach", state: "Virginia", zip: "81133", suite: "", notes: "", latitude: "37.444865", longitude: "-105.266293", user_id: 31, company_id: 31},
  {name: "Georgia Power", number: "10294", address: "241 Ralph McGill Blvd NE", city: "Atlanta", state: "GA", zip: "30308", suite: "", notes: "Lots of moolah!", latitude: "33.763574", longitude: "-84.380599", user_id: 1, company_id: 1}
])
Company.create!([
  {name: "Est Arcu Ac Foundation", address: "8897 Odio St.", state: "Delaware", city: "Pike Creek", zip: 97101, number: "(527) 581-4116", latitude: "45.095551", longitude: "-123.169011"},
  {name: "Integer Aliquam Adipiscing Associates", address: "Ap #299-8176 Lacus. St.", state: "Arkansas", city: "Fayetteville", zip: 71840, number: "(793) 147-9905", latitude: "33.383684", longitude: "-93.91407"},
  {name: "Vitae Risus Duis Foundation", address: "P.O. Box 644, 8292 Dolor. St.", state: "Minnesota", city: "Minneapolis", zip: 95401, number: "(724) 143-3307", latitude: "38.44097", longitude: "-122.797165"},
  {name: "Pharetra Felis Eget Corporation", address: "7800 Risus. Road", state: "Arizona", city: "Tucson", zip: 85254, number: "(110) 529-9473", latitude: "33.607183", longitude: "-111.940325"},
  {name: "Sit Amet Foundation", address: "116-5918 Quisque St.", state: "Utah", city: "Salt Lake City", zip: 26203, number: "(344) 646-2267", latitude: "38.536529", longitude: "-80.599385"},
  {name: "Ipsum Inc.", address: "P.O. Box 842, 9184 Felis. Avenue", state: "Missouri", city: "Kansas City", zip: 80626, number: "(247) 244-0903", latitude: nil, longitude: nil},
  {name: "Curae; Phasellus Industries", address: "P.O. Box 447, 3392 Rutrum Road", state: "Connecticut", city: "Waterbury", zip: 31973, number: "(611) 876-6136", latitude: nil, longitude: nil},
  {name: "Id Risus Quis Corporation", address: "Ap #257-5803 Euismod Av.", state: "Indiana", city: "Evansville", zip: 31529, number: "(883) 445-2689", latitude: nil, longitude: nil},
  {name: "Orci Consulting", address: "Ap #547-2401 Neque Rd.", state: "Alabama", city: "Birmingham", zip: 35466, number: "(172) 545-2124", latitude: nil, longitude: nil},
  {name: "Dis Parturient Montes Foundation", address: "1855 Eget Road", state: "Maine", city: "Bangor", zip: 28775, number: "(707) 414-4473", latitude: nil, longitude: nil},
  {name: "Sed Hendrerit A Incorporated", address: "Ap #948-1739 Maecenas Rd.", state: "Minnesota", city: "Minneapolis", zip: 80522, number: "(986) 699-8438", latitude: "40.583599", longitude: "-105.08148"},
  {name: "Vel Incorporated", address: "P.O. Box 267, 6410 Ligula Avenue", state: "Georgia", city: "Atlanta", zip: 45524, number: "(370) 300-4883", latitude: nil, longitude: nil}
])

Route.create!([
  {name: "test2", scheduled: "2015-07-06", user_id: 31, company_id: 1}
])
RouteStop.create!([
  {route_id: 4, account_id: 1, stop: 1},
  {route_id: 4, account_id: 17, stop: 2},
  {route_id: 4, account_id: 2, stop: 3}
])
