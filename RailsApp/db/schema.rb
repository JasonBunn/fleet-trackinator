# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150703061908) do

  create_table "accounts", force: true do |t|
    t.string   "name",                               null: false
    t.string   "number"
    t.string   "address",                            null: false
    t.string   "city",                               null: false
    t.string   "state",                              null: false
    t.string   "zip",                                null: false
    t.string   "suite"
    t.text     "notes"
    t.decimal  "latitude",   precision: 9, scale: 6
    t.decimal  "longitude",  precision: 9, scale: 6
    t.integer  "user_id",                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",                         null: false
  end

  add_index "accounts", ["company_id"], name: "index_accounts_on_company_id", using: :btree
  add_index "accounts", ["id"], name: "index_accounts_on_id", using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name",                               null: false
    t.string   "address",                            null: false
    t.string   "state",                              null: false
    t.string   "city",                               null: false
    t.integer  "zip",                                null: false
    t.string   "number"
    t.decimal  "latitude",   precision: 9, scale: 6
    t.decimal  "longitude",  precision: 9, scale: 6
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.decimal  "latitude",   precision: 9, scale: 6, null: false
    t.decimal  "longitude",  precision: 9, scale: 6, null: false
    t.integer  "user_id",                            null: false
    t.integer  "company_id",                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["company_id"], name: "index_locations_on_company_id", using: :btree
  add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: true do |t|
    t.integer "role_id", null: false
    t.integer "user_id", null: false
  end

  create_table "route_stops", force: true do |t|
    t.integer  "route_id",               null: false
    t.integer  "account_id",             null: false
    t.integer  "stop",       default: 1, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "route_stops", ["account_id"], name: "index_route_stops_on_account_id", using: :btree
  add_index "route_stops", ["route_id"], name: "index_route_stops_on_route_id", using: :btree

  create_table "routes", force: true do |t|
    t.string   "name",       null: false
    t.date     "scheduled",  null: false
    t.integer  "user_id",    null: false
    t.integer  "company_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "routes", ["company_id"], name: "index_routes_on_company_id", using: :btree
  add_index "routes", ["user_id"], name: "index_routes_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                                          default: "",        null: false
    t.string   "encrypted_password",                             default: "",        null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                  default: 0,         null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",                             default: "0.0.0.0", null: false
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.string   "first_name",                                                         null: false
    t.string   "last_name"
    t.integer  "company_id",                                                         null: false
    t.decimal  "latitude",               precision: 9, scale: 6
    t.decimal  "longitude",              precision: 9, scale: 6
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "version_associations", force: true do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

end
