class CreateCompanies < ActiveRecord::Migration
  def up
    create_table :companies do |t|
      t.string :name
      t.string :address
      t.string :state
      t.string :city
      t.integer :zip
      t.string :number
      t.decimal :latitude
      t.decimal :longitude

      t.timestamps
    end
  end
  
  def down
    drop_table :companies
  end
end