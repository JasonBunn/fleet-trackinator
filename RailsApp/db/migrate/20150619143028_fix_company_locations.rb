class FixCompanyLocations < ActiveRecord::Migration
  def change
    change_column :companies, :latitude, :decimal , precision: 9, scale: 6
    change_column :companies, :longitude, :decimal , precision: 9, scale: 6
    change_column :locations, :latitude, :decimal , precision: 9, scale: 6
    change_column :locations, :longitude, :decimal , precision: 9, scale: 6
    
    
    
  end
end
