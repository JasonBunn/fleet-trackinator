class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.belongs_to :user, index: true
      t.belongs_to :company, index: true

      t.timestamps
    end
  end
end
