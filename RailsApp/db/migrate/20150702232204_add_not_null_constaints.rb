class AddNotNullConstaints < ActiveRecord::Migration
  def change
    rename_column :routes, :schedualed, :scheduled
    change_column_null :accounts, :name, false
    change_column_null :accounts, :address, false
    change_column_null :accounts, :city, false
    change_column_null :accounts, :state, false
    change_column_null :accounts, :zip, false
    change_column_null :accounts, :user_id, false
    change_column_null :accounts, :company_id, false
    change_column_null :companies, :name, false
    change_column_null :companies, :address, false
    change_column_null :companies, :state, false
    change_column_null :companies, :city, false
    change_column_null :companies, :zip, false
    change_column_null :locations, :latitude, false
    change_column_null :locations, :longitude, false
    change_column_null :locations, :user_id, false
    change_column_null :locations, :company_id, false
    change_column_null :roles, :name, false
    change_column_null :roles_users, :role_id, false
    change_column_null :roles_users, :user_id, false
    change_column_null :routes, :name, false
    change_column_null :routes, :scheduled, false
    change_column_null :routes, :user_id, false
    change_column_null :routes, :company_id, false
    change_column_null :route_stops, :route_id, false
    change_column_null :route_stops, :account_id, false
    change_column_null :route_stops, :stop, false
    change_column_default :users, :current_sign_in_ip, '0.0.0.0'
    change_column_null :users, :current_sign_in_ip, false
    change_column_null :users, :first_name, false
    change_column_null :users, :company_id, false 
  end
end
