class CreateRouteStops < ActiveRecord::Migration
  def change
    create_table :route_stops do |t|
      t.belongs_to :route, index: true
      t.belongs_to :account, index: true
      t.integer :stop

      t.timestamps
    end
  end
end
