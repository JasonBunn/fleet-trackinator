class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.string :name
      t.date :schedualed
      t.belongs_to :user, index: true
      t.belongs_to :company, index: true

      t.timestamps
    end
  end
end
