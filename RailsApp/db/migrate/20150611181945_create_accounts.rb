class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.index :id
      t.string :name
      t.integer :number
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :suite
      t.text :notes
      t.decimal :latitude, precision: 9, scale: 6
      t.decimal :longitude, precision: 9, scale: 6
      t.references :user, index: true

      t.timestamps
    end
  end
end
