class ChangeNumberFormatInAccounts < ActiveRecord::Migration
  def change
    change_column :accounts, :number, :string
  end
  
  def down
      change_column :accounts, :number, :int
    end
end
