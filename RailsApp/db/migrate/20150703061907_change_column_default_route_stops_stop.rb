class ChangeColumnDefaultRouteStopsStop < ActiveRecord::Migration
  def change
    change_column_default :route_stops, :stop, 1
  end
end
