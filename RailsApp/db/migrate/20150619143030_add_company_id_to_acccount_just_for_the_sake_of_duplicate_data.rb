class AddCompanyIdToAcccountJustForTheSakeOfDuplicateData < ActiveRecord::Migration
  def change
  	add_reference :accounts, :company, index: true
  end
end
