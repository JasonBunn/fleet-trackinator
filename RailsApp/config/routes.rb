RailsApp::Application.routes.draw do

  get "map/index" => 'map#index', as: :map
  resources :routes

  resources :locations
  
  post "companies/new" => 'companies#new', as: :new_company_path
  resources :companies

  get "static/index"
  get 'static/about' => 'static#about', as: :about

  post "/admin/users/new" => 'admin/users#new'
  
  resources :accounts
  
  devise_for :users, :controllers => { registrations: 'users/registrations' } 
  
  namespace :admin do
    get '/' => 'users#index'

    resources :users
  end  
  
  namespace :manager do
    get '/' => 'users#index'

    resources :users
  end  
  post "/manager/users/new" => 'manager/users#new'
  
  resources :accounts do
      collection { post :search, to: 'accounts#index' }
  end 

 
  resources :route_stops
  controller :route_stops do
    post 'route_stops/new', :action =>  'new'
  end

  post "/users/location", to: 'admin/users#location'
  post "/map/users/location", to: 'admin/users#location'
  

  mount RailsAdmin::Engine => '/backend', as: 'rails_admin'
     
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root 'static#index'
  
  # get '/', to: redirect('/accounts')
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
