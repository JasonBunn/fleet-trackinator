# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsApp::Application.config.secret_key_base = 'ca96236c5ad94bf8165592113ab3abae87d5c64a953fe71f701d7390c63683cd6e577a8239c4ae5c3f4e876d66b1dcf564b81d68d755b75ba63e1fe8f874de14'
